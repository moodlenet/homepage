# Consent for data processing by the MoodleNet search index (aka "mothership")

[Moodle Pty Ltd](http://moodle.com) is a software company which develops and operates collaborative educational tools. [MoodleNet](http://moodle.net) is open source, and may be hosted by Moodle Pty Ltd, but also by anyone who wishes to manage an installation.

For instances _not_ hosted by Moodle Pty Ltd, they also provide the optional "mothership" API service that indexes public data as well as provides search and discovery across federated instances.

It is not compulsory for third party instances, such as ours, to link their instance of MoodleNet to the MoodleNet "mothership". However we have chosen to connect our instance, for the benefits we think it will offer.

Moodle Pty Ltd may therefore process your personal data for the purposes of offering additional functionality to the federated social network made up of individually hosted installations of MoodleNet software. This should lead to increased searchability, discovery, and sharing of resources and ideas amongst the educators using federated MoodleNet instances. Users will be able to find one another, discover relevant communities and collections, and find the resources they need.

This document exists to request you to give specific, valid and informed consent to the form of data processing this federation requires.

In [GDPR](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation) (General Data Protection Regulation 2016/679 of EU) terms adopted by Moodle Pty Ltd as global standard, their relationship with our instance is therefore both as:


-   Joint Data Controller (in respect of certain elements of personal data made public by users which Moodle Pty Ltd makes searchable across the federated instances connected to the MoodleNet "mothership")
-   Data Processor (Instance administrators act as the Data Controller, and can request Moodle Pty Ltd to delete personal data on the data subject from the MoodleNet "mothership")

The specific categories of data and forms of processing which will be completed by Moodle Pty Ltd will be the following:


| **CATEGORIES OF PERSONAL DATA**  | **WHAT COULD BE INFERRED FROM THE DATA** | **PURPOSE OF PROCESSING** | **THE SOURCE OF PERSONAL DATA** |
| --- | --- | --- | --- |
| **Display name, avatar, bio, interests, language, occupation, location, tags**   | workplace, origin, politicalopinions, religious beliefs, gender, sexuality, accessibility requirements  | User registration  | Data subject (user) |
| **Descriptions, language, metadata**   | interests, occupation, location, workplace, location, origin, politicalopinions, religious beliefs, gender, sexuality, accessibility requirements | Describing communities, collections, or shared resources | Data subject (user) |
| **IP address**   | geolocation of connection to the service  | Connecting users to the service  | Data subject (user) |
| **Details about browser and device**   | accessibility requirements  | Ensuring the service is accessible to all users  | Data subject (user) |
| **Search history**   | political beliefs, religious beliefs, sexuality, accessibility requirements | Improving user experience  | Data subject (user) |


If you choose to enter any information involving a special category of personal data (for example, entering details about your racial or ethnic origin, political opinions, religious or philosophical beliefs, or trade union membership, and the processing of genetic data, biometric data for the purpose of uniquely identifying a natural person, data concerning health or data concerning your sex life or sexual orientation) in your own personal profile, in comments, or anywhere else on this site, then we and Moodle Pty Ltd will rely on this informed consent to be able to process that data.

You have the right to withdraw consent for the processing of personal data at any time by contacting the admin of this instance, or failing that, by notifying Moodle Pty Ltd on <mailto:privacy@moodle.com>.

