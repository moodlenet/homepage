# MoodleNet User Agreement


### 1. Terms

By accessing or using this MoodleNet instance (an installation of MoodleNet, a social web application that can connect you with people and communities on any instance of MoodleNet or other federated apps, aka the "fediverse"), you are agreeing to be bound by these terms, and all applicable laws and regulations. If you do not agree with any of these terms, you are prohibited from using or accessing this instance.


### 2. Code of Conduct

#### 2.1 Pledge

In the interest of fostering an open and welcoming environment, we as contributors and project maintainers pledge to make participation in our project and our community a harassment-free experience for everyone, regardless of age, body size, disability, ethnicity, sex characteristics, gender identity and expression, level of experience, education, socio-economic status, nationality, personal appearance, race, religion, or sexual identity and orientation.

#### 2.2 Encouraged Behaviour

Examples of behaviour that contributes to creating a positive environment include:

-   Using welcoming and inclusive language
-   Being respectful of differing viewpoints and experiences
-   Gracefully accepting constructive criticism
-   Focusing on what is best for the community
-   Showing empathy towards other community members

#### 2.3 Unacceptable Behaviour

Racism, sexism, homophobia, transphobia, harassment, defamation, doxxing, sexual depictions of children, and conduct promoting alt-right and fascist ideologies will not be tolerated.

Other examples of unacceptable behaviour by participants include:

-   Unwelcome sexual attention or advances
-   Trolling, insulting/derogatory comments, and personal or political attacks
-   Public or private harassment
-   Publishing others’ private information, such as a physical or electronic address, without explicit permission
-   Other conduct which could reasonably be considered inappropriate in a professional setting

#### 2.4 Responsibilities of users and contributors

Users and contributors are responsible for watching out for any unacceptable behaviour or content, such as harassment, and bringing it to moderators' attention by using the flagging functionality. If community moderators do not respond in a timely or appropriate manner, users are to alert the instance administrators, and failing that,Moodle Pty Ltd “Moodle HQ” at <mailto:moodlenet-moderators@moodle.com>.

#### 2.5 Responsibilities of the project maintainers

Project maintainers (including instanceadministrators, community moderators, and Moodle HQ, given the relevant access) are responsible for monitoring and acting on flagged content and other user reports, and have the right and responsibility to remove, edit, or reject comments, communities, collections, resources, images, code, wiki edits, issues, and other contributions that are not aligned to this Code of Conduct, or to suspend, block or ban (temporarily or permanently) any contributor, community, or instance for breaking these terms, or for other behaviours that they deem inappropriate, threatening, offensive, or harmful.

Instanceadministrators should ensure that every community hosted on the instance is properly moderated according to the Code of Conduct.

Project maintainers are responsible for clarifying the standards of acceptable behaviour and are expected to take appropriate and fair corrective action in response to any unacceptable behaviour.

#### 2.6 Enforcement

All complaints must be reviewed and investigated by project maintainers, and should result in a response that is deemed necessary and appropriate to the circumstances. Project maintainers are obligated to maintain confidentiality with regard to the reporter of an incident. Further details of specific enforcement policies may be posted separately.

Project maintainers who do not follow or enforce the Code of Conduct in good faith may face temporary or permanent repercussions as determined by other project maintainers.

#### 2.7 Code of Conduct Attribution

This Code of Conduct was adapted from the [ Contributor Covenant](https://www.contributor-covenant.org/) ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)), [version 1.4](https://www.contributor-covenant.org/version/1/4/code-of-conduct.html).


### 3. Contribution, Use, Modification and Distribution Licenses

1.  The software and materials contained in this website are protected by applicable copyright law and open source or similar licences as indicated.
2.  Unless otherwise noted, all content or materials contributed on this instance (including resources, metadata, and contents of public discussions) is made available under a [Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) license](https://creativecommons.org/licenses/by-sa/4.0/). This does not necessarily include links to external instances or websites.
3.  MoodleNet is powered by free software, meaning that you have the following basic freedoms:

    -   The freedom to run the software as you wish, for any purpose.
    -   The freedom to study how the software works, and change it so it does your computing as you wish. Access to the source code is a precondition for this.
    -   The freedom to redistribute copies so you can help others.
    -   The freedom to distribute copies of your modified versions to others. By doing this you can give the whole community a chance to benefit from your changes.

4.  Permission is granted to run, study, redistribute, and distribute modified copies of the MoodleNet software according to the terms of the [GNU Affero Public License 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html) (“AGPL”). Note that this is different to the GPL license used for Moodle Core. The AGPL mandates that the source of your MoodleNet instance must be available to be downloaded even if you are providing a service rather than making available a downloadable app. Further information is available at [tl;drlegal](https://tldrlegal.com/license/gnu-affero-general-public-license-v3-(agpl-3.0)).


### 4. Disclaimers

#### 4.1. Materials provided 'as is'

The materials on this instance (and on the "fediverse" at-large) have been contributed by other users, and are provided on an 'as is' basis. Neither the instance administrators, nor Moodle HQ, make any warranties, expressed or implied. They hereby disclaim and negate all other warranties including, without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights.

#### 4.2. Accuracy

Furthermore, neither the instance administrators, nor Moodle HQ, make any representations concerning the accuracy, likely results, or reliability of use of the materials found on this instance - which may be incomplete or outdated, or could include technical, typographical, or photographic errors - or relating to such materials or on any sites linked from this instance.

Changes may be made to the materials contained on its instance at any time without notice. However, neither the instance administrators nor Moodle HQ make any commitment to update the materials.

#### 4.3. Links

Neither the instance administrators nor Moodle HQ have reviewed all of the websites, pages or resources linked or uploaded on this instance (or on the "fediverse" at-large) and are not responsible for the contents of any such links. The inclusion of any link does not imply endorsement of the site. Use of any such linked resource is at the user's own risk.

#### 4.4. Limitations

In no event shall the instance administrators, Moodle HQ, or their suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption) arising out of the use or inability to use the materials on this instance (or on the "fediverse" at-large), even if any authorised representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.

### 5. Modifications

These terms of service may be revised at any time without notice, though the instance administrators will be notified of any significant change, and in turn they should notify you. By continuing to use this instance, you are agreeing to be bound by the published current version of these terms. For the avoidance of any doubt, these terms are applicable in addition to the terms that may be added by the instance administrators. In any case those instance-specific terms shall not reduce or replace these terms and in case of conflict these terms shall prevail.

