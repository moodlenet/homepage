---
title: Consent for data processing by the MoodleNet search index (aka "mothership")
sidebar: false 
include_footer: true
---

{{< include_md file="/static/terms/indexing.md" >}} 