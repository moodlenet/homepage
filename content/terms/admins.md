---
title: MoodleNet Mothership Terms & Conditions for Instance Administrators
sidebar: false 
include_footer: true
---

{{< include_md file="/static/terms/admins.md" >}}  