# MoodleNet Mothership Terms & Conditions for Instance Administrators


MoodleNet is a federated social network. As such, we cannot control everything that happens on the network, but nevertheless commit to make our best effort to guarantee a safe experience to MoodleNet users. For this reason we have created a MoodleNet Mothership Server Terms & Conditions in order to provide people a safe and friendly experience without compromising the open and federated nature of the project.

Administrators of MoodleNet servers (“instances”) have the option of connecting to Moodle HQ’s ‘Mothership’. This means that content stored on those instances appears both in network-wide search results, and in recommendations to other users.

To connect to the Mothership, administrators of MoodleNet instances **must commit to the following:**


1.  **Foster an open and welcoming environment.** All of us (Moodle HQ, instance administrators, community moderators, and all MoodleNet participants) need to work together to maintain a harassment-free experience for everyone, regardless of age, body size, disability, ethnicity, sex characteristics, gender identity and expression, level of experience, education, socio-economic status, nationality, personal appearance, race, religion, or sexual identity and orientation.  
      

2.  **Actively moderate their instance** against racism, sexism, homophobia, transphobia, harassment, defamation, doxxing, sexual depictions of children, and conduct promoting alt-right and fascist ideologies. We want to give users the confidence that they are joining a safe place, and as such we cannot support or promote anyone spreading intolerance and hate. A [MoodleNet User Agreement](/terms/users/index.html) has been written for this purpose. Instance administrators must **monitor and be responsive to flagged content** and other user reports, and make sure every community hosted on the instance is properly moderated. 


3.  **Perform daily backups.** This is a basic necessity to protect from loss of user data. We recommend storing an archive of daily, weekly, and monthly backups.


4.  **Give emergency access to the server infrastructure to at least two people.** The ability for others to access the MoodleNet server is a key aspect to address technical emergencies.


5.  **Obtain valid consent of users**, this is required in order to provide MoodleNet service and process users personal data as part of the Moodle HQ’s ‘Mothership’. A document to obtain [consent for data processing by the MoodleNet search index (aka "mothership")](/terms/indexing/index.html) has been prepared for this purpose.

  
6.  **Give users at least 3 months of advance warning in case of shutting down.** Users must have the confidence that their account will not suddenly disappear and therefore is vital to give them the time and the chance to export their data and migrate to another instance.

  
7.  **Make available the source code of any customisations to your instance**, regardless how small. This is required by the AGPL licence anyway, but is always worth repeating.


If you are a MoodleNet instance administrator and can commit to the above, you can gain an API key. Please note that only instances that accept registrations from the public will be shown in the instance picker.

